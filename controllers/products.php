<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Products extends Public_Controller 
{

	/**
	 * 
	 * @constructor
	 */
	public function __construct() 
	{
		parent::__construct();
		
		// Retrieve some core settings
		$this->shop_title = Settings::get('ss_name');		//Get the shop name
		
		// Load required classes
		$this->load->model('products_front_m');
		$this->limit = Settings::get('ss_qty_perpage_limit_front');


	}
	


	/**
	 *
	 * This displays the list of ALL products.
	 *
	 * @description -Products Controller to view all Products
	 * @uri yourdomain.com/shop/products
	 *
	 */
	public function index( $offset = 0 ) 
	{

		$data = new stdClass;


 		$filter = array();
	

		// 
		//  Count total items by the given filter
		// 
		$total_items = $this->products_front_m->filter_count($filter);


		// 
		//  Build pagination for these items
		// 
		$data->pagination = create_pagination( 'shop/products/' , $total_items, $this->limit, 3);

		

		//
		// finally
		//
		$this->template
			->title(lang('shop:label:shop').' |' .lang('shop:label:products'))
			->set_breadcrumb($this->shop_title)
			->set('product_count',$total_items)	
			->set('category_id','0')
			->set('limit', $data->pagination['limit'] )	
			->set('offset', $data->pagination['offset'] )				
			->build('common/products_list', $data);
			
	}


	/**
	 * @param   [admin_as_customer]  Bool set to 'customer' if you are an admin and want to view the product as a customer
	 * @description If the system doesnt find the product it will redirect away
	 * EX : domain.com/products/product/7/customer  
	 *
	 *
	 * OPTION 1 : domain.com/products/product/slug   
	 * OPTION 2 : domain.com/product/slug  
	 * OPTION 3 : domain.com/products/product/7   	
	 * OPTION 4 : domain.com/product/7  		 
	 */
	public function product($param = '', $admin_as_customer = FALSE) 
	{
	
		$data = (object) array();
		
		// Load required classes
		$this->load->model('products_front_m');
		
		

		// Determine whether to get by id or slug
		$method = (is_numeric($param))  ? 'id' : 'slug' ;

		

		// $data->product = $this->products_front_m->get($param, id|slug , increment(BOOL) );
		$data->product = $this->products_front_m->get($param, $method, TRUE );
		

		// if product does not exist, redirect away
		if( (!$data->product) OR ($admin_as_customer == 'customer' && group_has_role('shop', 'admin_products') ) )
		{

			if ( $data->product->date_archived != NULL ) 
			{
					// Display message that product is not currently available
					$this->template
						->title($this->module_details['name'])						

						//we do not want to send all the data object, only the name as the product is hidden from customer view		
						->build('special/product_deleted', array('product_name' => $data->product->name) ); 

					return;
			}	

			if ($data->product->public == ProductVisibility::Invisible )
			{
					// Display message that product is not currently available
					$this->template
						->title($this->module_details['name'])						

						//we do not want to send all the data object, only the name as the product is hidden from customer view		
						->build('special/product_invisible', array('product_name' => $data->product->name) ); 

					return;
					
			}

		}



		if(group_has_role('shop', 'admin_products') && $admin_as_customer == FALSE)
		{
			$site_url = base_url();
			

			$notmsg = 'You are viewing this product as an Administrator &bull; <a target="_new" href="'.$site_url.'admin/shop/product/edit/'.$param.'">click here to edit this item</a>';
			$this->session->set_flashdata('error', $notmsg );


			if($data->product->public == 0)
			{
				$this->session->set_flashdata('error', lang('shop:messages:product_is_hidden') );
			}

			//is the product deleted
			if($data->product->date_archived != NULL)
			{
				$this->session->set_flashdata('error', lang('shop:messages:product_is_deleted') );
			}			
		}



		// We need to get the specific view for this product
		$_view_file = $this->_get_view_file($data->product);


		// Display the product
		$this->template
			->title($this->module_details['name'].' &rarr;'.$data->product->name)		
			->set_breadcrumb($data->product->name) // current crumb
			/*if seo is installed */
			/*->set_metadata('description', strip_tags($data->product->meta_desc)) */						
			->build($_view_file, $data); 

	}	



   /**
	* List all products by a category
	*
	* We have to check if module is installed
	*/
	public function category( $category = 0, $offset = 0) 
	{
		$this->load->model('shop/products_front_m');
		$this->load->model('shop_categories/categories_m');
		
		//initialize
		$data = (object) array();

		//id or slug
		if ( is_numeric($category) )
		{
 			$category = $this->categories_m->get_by('id', $category) ;
		}
		else
		{
			$category = $this->categories_m->get_by( 'slug', $category) ;
		}

		$catid = 0;
	
		//if the category exist
		if($category)
		{

			$uri = 'shop/categories/category/' . $category->slug;

			$filter['category_id'] = $category->id;
			$catid = $category->id;

			// Count the items
			$total_items = $this->products_front_m->filter_count($filter);

			$data->pagination = create_pagination( $uri, $total_items, $this->limit, 5);

			//Get the items for the display
			//$data->products = $this->products_front_m->filter($filter, $data->pagination['limit'] , $data->pagination['offset']);		


		}
		else
		{
			$data->products = NULL;
			
		}

		
		$this->template
			->title($this->module_details['name'].' |' .lang('shop:label:products'))
			->set_breadcrumb($this->shop_title)
			->set('limit', $data->pagination['limit'] )	
			->set('offset', $data->pagination['offset'] )				
			->set('category_id',$catid)
			->set('product_count',$total_items)
			->build('common/products_list', $data);

	}

	/**
	 * 
	 * 1st will get by the set view in product_databse
	 * 2nd will try to get a thview from the layout by product slug
	 * 3rd will return the default products_single (default) which is always available
	 * 
	 * @param  [type] $product_slug [description]
	 * @return [type]               [description]
	 */
	private function _get_view_file($product)
	{

		$view_name = 'products_single_' . $product->page_design_layout;
		
		$this->load->library('shop/design_library');


		$path = $this->design_library->get_custom_path()  . '/' . $view_name;


		if(file_exists($path. '.php'))
		{
			return 'custom/'.$view_name;
		}	
		else
		{
			return 'common/products_single';
		}

	}


}