<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Cart extends Public_Controller 
{

	// List of messages (error|success) for return
	private $_MESSAGES = array();


	public function __construct() 
	{
		parent::__construct();
		

		// Retrieve some core settings
		$this->shop_title = Settings::get('ss_name');		//Get the shop name
		$this->allow_guest = Settings::get('ss_allow_guest_checkout');
		$this->has_logged_in_user =  ($this->current_user)? TRUE : FALSE ;


		$this->_init_messages();
		
				
		// Load required classes
		$this->load->model('products_front_m');

		
	}


	private function _init_messages()
	{

		// Success add message
		$this->_MESSAGES[101] = lang('shop:cart:item_added'); 

		// Failed to add messagea
		$this->_MESSAGES[200] = lang('shop:cart:item_not_added'); 
		$this->_MESSAGES[201] = lang('shop:cart:id_qty_not_set');
		$this->_MESSAGES[202] = lang('shop:cart:product_not_found');
		$this->_MESSAGES[203] = lang('shop:cart:product_not_available');
		$this->_MESSAGES[204] = lang('shop:cart:product_out_of_stock');
		$this->_MESSAGES[210] = lang('shop:cart:you_must_login_before_shopping');


		// Item removed messages
		$this->_MESSAGES[300] = lang('shop:cart:item_removed1'); 
		$this->_MESSAGES[301] = lang('shop:cart:item_removed2');		
		$this->_MESSAGES[302] = lang('shop:cart:not_in_cart');	

	}



	/**
	 * Display Cart - The cart content is accessed via the plugin
	 */
	public function index() 
	{

		$this->template->title($this->module_details['name'])
				->build('common/cart');

	}

	/**
	 * Redirect after success
	 *
	 * @param INT $id
	 * @param INT $qty
	 *
	 * @access public
	 */
	public function add($id = 0, $qty = 1) 
	{
		//
		// Message handling for ajax
		//
		$sys_message = array();
		$sys_message['status'] = JSONStatus::Error;
		$sys_message['message'] = 'Unknown';
		$sys_message['cost'] = 0.00;
		$sys_message['qty'] = 0;
	
	
		$url_redir = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'shop/cart';
		

		// Check the post header to see if the item come from a post or a direct link
		if( $this->input->post('id') ) 
		{
		
			// Get the product ID
			$id = $this->input->post('id');
			
			// The POST must contain the QTY
			$qty = intval( $this->input->post('quantity') );

			if(!$qty)
			{
				// Accept either qty or quantity
				$qty = intval( $this->input->post('qty') );

				//final check, if not set then set as 1 as the qty
				$qty = ($qty)?$qty:1;
				
			}
			
		}
		
		
		
		//
		// pre-Add checks
		//
		$product = $this->_add($id, $qty);

		if( is_object($product) == FALSE )
		{

			$message =  $this->_MESSAGES[$product];

			if($this->input->is_ajax_request())
			{
				$sys_message['message'] = $message;
				echo json_encode($sys_message);die;
			}
			else
			{
				// if the product/ request faled to validate just redirect now
				$this->session->set_flashdata( JSONStatus::Error , $message );
				redirect($url_redir);
			}
		}







		// 
		// (WIP) Get all the options the user has selected 
		// 

		$options = array();

		//
		// 
		//


  
	
   

		// Prepare the item for the cart 
		$data = $this->_prepare_item_for_cart($product, $qty, $options);

		
		
		//
		// Add to cart
		//
		$success = $this->sfcart->insert($data);
		


		//
		// Trigger Event to Notify User of status (success/Failer)
		//
		Events::trigger('evt_cart_item_added', array('id' => $id, 'name' => $data['name'], 'success' => $success) );



		
		//
		// Format return message
		//
		if($success)
		{
			$_MESSAGE_TEXT = 101;
			$_MESSAGE_STATUS  = JSONStatus::Success;
		}
		else
		{
			$_MESSAGE_TEXT = 200;
			$_MESSAGE_STATUS  = JSONStatus::Error;
		}


		$message = sprintf( $this->_MESSAGES[$_MESSAGE_TEXT] , $product->name ); 


		//
		// Return data/messages
		//
		if($this->input->is_ajax_request())
		{
			
			//prepare ajax return statement
			$total_items = $this->sfcart->total_items();

			if($total_items==NULL) 
				$total_items = 0;

			$sys_message['status'] = $_MESSAGE_STATUS;
			$sys_message['qty'] = $total_items; //total items in cart
			$sys_message['cost'] =  number_format( (float) $this->sfcart->total_cost_contents() , 2); //cost of cart
			$sys_message['message'] = $message;

			echo json_encode($sys_message);return;
		}
		else
		{

			$this->session->set_flashdata( $_MESSAGE_STATUS , $message );
			
			//
			// redirect them back to page
			//
			redirect($url_redir);
		}
	}
	
	

	/**
	 * Redirect after success
	 *
	 * @param INT $id
	 * @param INT $qty
	 *
	 * @access public
	 */
	public function _add($id = 0, $qty = 1) 
	{
	
	

		//
		// Check id the product ID and QTY values are OK
		//
		if( (!$id ) OR (!$qty) ) 
		{
			//Product ID or QTY was not set
			return 201;
		}


		//
		// Check that ID/QTY is a valid int, do not allow anything else other than an INT
		//
		$id = intval($id);
		$qty = intval($qty);



		// Get product from DB
		$item = $this->products_front_m->get($id,'id');  

		
		
		//
		// Check if product exist or still available
		//
		if(!$item)
		{
			return 202;
		}

		
	
		//
		// Check product validady (visible or deleted) 
		// 
		// This is important to check because in products_front_m if the user is admin it does not check this.
		// So it is a must at this stage.
		//
		if( is_deleted($item) || ($item->public === ProductVisibility::Invisible ) )
		{
			return 203;
		}
		
			
		//
		// Check for inventory levels
		//
		if(!($this->_check_inventory($item, $qty)) ) 
		{
			return 204;
		}
	


		//
		// Check if User req to login
		//
		//if ( $this->login_required && !$this->has_logged_in_user) 
		//{
		//	return 210;
		//}


		// 
		// If we have reached this point then we can validate successfully
		//
		return $item;

	
	}
	
	
	/**
	 * update()
	 *
	 * 
	 * @access public
	 */
	public function update() 
	{
	
		// Lets get some validation here
		$data = $this->input->post();
		$update_data = array();
		

		unset($data['update_cart']);

		foreach ($data as $d_item) 
		{
			
			$update_item = array();

			$update_item['rowid'] = $d_item['rowid'];
			$update_item['id'] = $d_item['id'];
			$update_item['qty'] = $d_item['qty'];
			

			$item = $this->products_front_m->get( $d_item['id'] );
			if ($item) 
			{

				$update_item['price'] = $item->price;

				$update_data[] = $update_item;

			}

		}

		//apply possible qty changes and dletes
		$result = $this->sfcart->update($update_data);
		 
		
		redirect('shop/cart');
	}




	/**
	 * The update checks that all values are set properly, 
	 * if not it removes the key from the array
	 *
	 */
	private function _update() 
	{
	
		// Lets get some validation here
		$data = $this->input->post();
		$update_data = array();
		

		foreach ($data as $key => $d_item) 
		{
			
			$update_item = array();


			if( isset($d_item['id']) )
			{

				if( isset($d_item['rowid']) )
				{
					if( isset($d_item['qty']) )
					{
						continue;
					}

				}

			}

			//unset invalid update data
			unset($data[$key]); 

		}

		return $data;


	}





	
	/**
	 * Delete an item from the cart
	 *
	 * @param String RowId of product in cart
	 * @access public
	 */
	public function delete($rowid) 
	{

		$url_redir = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'shop/cart';


		$items = $this->sfcart->contents();

		if($items[$rowid])
		{
			//found :)
			//get the name
			$prod_name = $items[$rowid]['name'];
			$this->sfcart->remove($rowid);
			$_MESSAGE_CODE = 301;
			$_STATUS = JSONStatus::Success;

		}
		else
		{
			// item does not exist in cart
			$prod_name = lang('shop:cart:unknown');
			$_MESSAGE_CODE = 302;
			$_STATUS = JSONStatus::Error;
		}




		$message = sprintf( $this->_MESSAGES[$_MESSAGE_CODE] , $prod_name );




		if($this->input->is_ajax_request())
		{
			$total_items = $this->sfcart->total_items();

			if($total_items==NULL) 
				$total_items = 0;


			$sys_message['qty'] = $total_items; //total items in cart
			$sys_message['cost'] =  number_format( (float) $this->sfcart->total_cost_contents() , 2); //cost of cart
			$sys_message['message'] = $message;
			$sys_message['status'] = $_STATUS;

			echo json_encode($sys_message);die;
			
		}
		else
		{
			// if the product/ request faled to validate just redirect now
			$this->session->set_flashdata( $_STATUS , $message );
			redirect($url_redir);
		}
		
	}

	
	/**
	 * This will clear the entire cart of its contents
	 * and shipping values / a full reset.
	 *
	 *
	 */
	public function drop() 
	{
	
		$this->sfcart->destroy();
		
		if($this->input->is_ajax_request())
		{
			die("Cart Destroyed");
		}
		else
		{
			redirect('shop/cart');
		}

	}
	
	
	/**
	 * This function checks wheather the requested stock item
	 * is in stock and has enough inventory for the request add
	 * @param unknown_type $item
	 * @access private
	 */
	private function _check_inventory($item, $qty = 1) 
	{
		
		// Check 1: only allow the in-stock status
		if( $item->status !== InventoryStatus::InStock )
		{
			//no stock
			return FALSE;
		}
		
		// If unlimited stock - no further validation required
		if (($item->inventory_type == InventoryType::Unlimited )) return TRUE;

					
		// Everything below here is subject to Qty availability
		if (($item->inventory_on_hand >= $qty)) return TRUE;

				
		// Anything below here we just dont have the stock!!
		return FALSE;

	}
	

	/**
	 * 
	 * @param unknown_type $item
	 * @param unknown_type $qty
	 * @param unknown_type $options
	 * @return multitype:unknown NULL mixed
	 */
	private function _prepare_item_for_cart($item, $qty=1, $options=array()) 
	{

		// Clean name from bad characters
		$name =  convert_accented_characters( $item->name );
		
		// Assign the Cart item array
		$data = array(
				'id' => $item->id,
				'slug' => $item->slug,
				'qty' => $qty,
				'price' => $item->price,
				'name' => $name,
				'options' => $options,
				'requires_shipping' => ($item->req_shipping)? 0 : 1 ,		
		);
		
		return $data;
		
	}

	

}