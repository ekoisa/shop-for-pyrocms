<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Payment extends Public_Controller 
{

	// Support multiple checkout theme/styles
	protected $theme_name = 'single';
	
	public function __construct() 
	{
		parent::__construct();

    
        $this->load->model('orders_m');
        $this->load->library('gateway_library');
        $this->load->model('addresses_m');
        $this->lang->load('merchant');
        $this->load->library('merchant');
        $this->load->model('transactions_m');
  

	}


       

    /**
     * This displays the facility to pay for an order based on the selected gateway set in the order params
     */
    public function order($order_id = 0, $trace = false) 
    {

        $data = (object) array();


        $data->order = $this->orders_m->get($order_id);


        // Get the gateway from the order
        $data->gateway = $this->gateway_library->get( $data->order->gateway_id );

        if( ($data->gateway->options['auto']==1)  )
        {
            //skip this page and just process
            $this->process( $order_id );
            return;

        }
    


        // Display the gateway page with their own option (if they have any)
        $this->template
            ->title($this->module_details['name'], lang('shop:label:customer'))
            ->build( $data->gateway->view_path() , $data);



    }

    public function process($order_id = 0, $trace = false) 
    {

        $data = (object) array();


        $data->order = $this->orders_m->get($order_id);

        $retval = $this->_validate_callback_req($data->order);
        if($retval['status'] == false)
        {
            redirect($retval['redir']);
        }


        // Get the gateway from the order
        $data->gateway = $this->gateway_library->get( $data->order->gateway_id );



        // Initialize CI-Merchant
        $this->merchant->load( $data->gateway->slug );


        // Initialize the Merchant Object with Settings
        $this->merchant->initialize( $data->gateway->options ); 


        
        // Prepare items for the merchant library - This will itemize order with totals
        $payable_items = $this->prep_items_for_merchant( $data->order );



        $billing_addr   = $this->addresses_m->get($data->order->billing_address_id);



        $params = $data->gateway->get_param($billing_addr, $payable_items, $data->order, Settings::get('ss_currency_code') );

      

        //  Store the params as JSON in the order field
        $this->orders_m->update_payment_settings( $data->order->id, $params );


        //process the order before sending to merchant library
        $data->gateway->process( $data->order );

        //on this line depending on the merchat we are redirected or returnto this line of execution
        $response  = $this->merchant->purchase($params);



        // If we reach here we need to handle the response from the payment method.            
        $this->_handle_payment_return_response( $data->order,  $data->gateway, $response );
            
    

        redirect('shop/');            

        
    
    }


    //
    // base_url()/shop/payment/callback{{order-id}}
    //
	public function callback($order_id)
	{

        $this->load->library('session');


        // We have to re-instate the sesssion from the order
        $data = new stdClass();
		$data->order 	= $this->orders_m->get($order_id);


        // Order paid, lets bypass here 
        $retval = $this->_validate_callback_req($data->order);
        if($retval['status'] == false)
        {
            //redirect($retval['redir']);
            redirect($retval['redir']);
        }

		$data->gateway 	= $this->gateway_library->get( $data->order->gateway_id );
        $this->merchant->load( $data->gateway->slug );
        $this->merchant->initialize( $data->gateway->options );


        // Get the params for this order
        $params = (array) json_decode($data->order->data);


        // Calculate response headers
        $response = $this->merchant->purchase_return( $params );


	    // Gateway specif code
		$data->gateway->callback($response);


  
        //records transactions
        $this->_handle_payment_return_response( $data->order, $data->gateway, $response );



		// Let everyone know
		Events::trigger('evt_gateway_callback', array() );



        //now lets redirect to the order view
        if($this->current_user)
        {
		  redirect('shop/my/orders/order/'. $data->order->id );
        }



        redirect('shop/guest/order2/'. $data->order->id . '/' . md5($data->order->pin) );


	}






	/**
	 * Handles cancel from payment gateway
	 *
	 *
	 */
	public function cancel($order_id) 
	{


        $data_field = json_encode( isset($_POST) ?  $_POST  :  $_GET );

        $this->transactions_m->gateway_cancel( $order_id, $data_field  );   



		if (strtolower(substr(current_url(), 4, 1)) == 's') 
		{
			redirect(str_replace('https:', 'http:', site_url('shop')) . '?session=' . session_id());
		}
		else
		{
			$this->session->set_flashdata('notice', lang('shop:payments:payment_cancelled'));
			redirect('shop');
		}
	}





    private function _handle_payment_return_response( $order, $gateway, $response )
    {


        $data_field = array();
        $data_field['Merchant Reference'] = $response->reference();
        $data_field['Merchant Data'] = $response->data();
        $data_field['Merchant Message'] = $response->message();
        $data_field['Merchant Status'] = $response->status();     


        switch($response->status())
        {
            case Merchant_response::COMPLETE:
                $set_status = OrderStatus::Paid ;
                $this->orders_m->mark_as_paid( $order->id );
                $this->transactions_m->purchase_paid( $order->id , $order->cost_total, 0, $gateway, $response->status() , json_encode($data_field) ); 
                Events::trigger('evt_order_paid',  $order->id );
                break;
            case Merchant_response::REDIRECT:
            case Merchant_response::REFUNDED:
            case Merchant_response::FAILED:
            case Merchant_response::AUTHORIZED:            
            default:
                $set_status = OrderStatus::Pending ;
                $this->transactions_m->purchase_return( $order->id,  $gateway, $response->status() , json_encode($data_field)  );    
                break;
        }        

        // Set the order status
        $this->orders_m->set_status( $order->id , $set_status );

       
    }

	public function _validate_callback_req($order)
	{

        $retval = array();

        $retval['status'] = true;
        $retval['redir'] = 'shop';

       
		//Does order exist
		if( ! $order )
		{
			$this->session->set_flashdata(lang('shop:payments:order_does_not_exist'));
			$retval['status'] = false;
		}
        else
        {
            //
            if( $order->status === OrderStatus::Paid )
            {
                $this->session->set_flashdata(lang('shop:payments:order_has_been_paid'));
                $retval['status'] = false;
                $retval['redir'] = 'shop/my/orders/order/'.$order->id ;
            }   

        }
        
		return  $retval;

	}

    private function prep_items_for_merchant( $order )
    {

        //order_m should already be loaded
        $items = $this->orders_m->get_order_items( $order->id );


        $payable_items = array();        

        //add the items
        foreach($items as $item)
        {
             $payable_items[] =  array( 

                  'name'=> $item->name,
                  'desc'=> '',/*$item->description,*/
                  'amt' => (float) $item->cost_item,
                  'qty' => $item->qty,
                );
        }


        if( $order->cost_shipping ) //If cost of shipping > 0
        {
            //add the shipping
            $payable_items[] =  array( 

                  'name'=>'Shipping and Handling',
                  'desc'=> '',/*$item->description,*/
                  'amt' => (float) $order->cost_shipping,
                  'qty' => 1,
            );

        }  


        return $payable_items;  
    }    

}

