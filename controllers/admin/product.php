<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */

//include_once( dirname(__FILE__) . '/' . 'products_admin_controller.php');

class Product extends Admin_Controller 
{

	protected $section = 'products';
	

	public function __construct() 
	{
		parent::__construct();

		// Do not allow users to edit products
		role_or_die('shop', 'admin_products');		

		// Create the data object
		$this->data = new stdClass();


		// Load all the required classes
		$this->load->model('products_admin_m');
		$this->load->model('shop_categories/categories_m');
		$this->load->model('shop_brands/brands_m');
		$this->load->model('tax_m');
		$this->load->helper('url');
		$this->load->library('session'); 

		$this->load->library('form_validation');
		$this->load->library('keywords/keywords');
		
		

		// Set the validation rules
		$this->_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'lang:name',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'slug',
				'label' => 'lang:slug',
				'rules' => 'trim|required'
			),				
			array(
				'field' => 'price',
				'label' => 'lang:price',
				'rules' => 'trim|numeric'
			),	  			 			 		
			array(
				'field' => 'status',
				'label' => 'lang:status',
				'rules' => 'trim'
			),

		);


		$this->mod_path = base_url() . $this->module_details['path'];


		Events::trigger('evt_admin_load_assests');
		
		// For all pages on products associate the fllowing files/settings
		$this->template
					->append_metadata('<script></script>')
					->append_metadata('<script type="text/javascript">' . "\n  var MOD_PATH = '" . $this->mod_path . "';" . "\n</script>");		

	}


	private function getInstalledModules() 
	{

		$arr = array();
		$this->load->model('modules/module_m');

		//List of all SHOP modules compatible with core
		$array_of_modules = array(
			'shop_attributes',
			'shop_options',
			'shop_categories',
			'shop_related',
			'shop_digitalfiles',
			'shop_images',
			'shop_seo',
			'shop_inventory'
		);

		foreach($array_of_modules as $_mod)
		{
			$module = $this->module_m->get_by('slug', $_mod);
	    	$arr[$_mod] = $module->installed;	
		}

	
    	return $arr;
	}



	public function index() 
	{
		return NULL;	
	}	





	/**
	 * Create a new Product
	 * 
	 */
	public function create() 
	{

		// Prepare for postback
		// Setup extra validation rules not applied to the main set
		$this->form_validation->set_rules($this->_validation_rules);
		$this->form_validation->set_rules('slug', 'lang:slug', 'trim|max_length[100]|required|is_unique[shop_products.slug]');

		$this->data->price = 0;
		$this->data->slug ='';
		$this->data->name = '';

		// If postback validate the form
		if ($this->form_validation->run()) 
		{

			$input = $this->input->post();

		  
			// create enables the creating of a product with basic param, after created
			// we can use edit to assign images ect.
			if ($product_id = $this->products_admin_m->create($input)) 
			{
							
				Events::trigger('evt_product_created', $product_id);	
				$this->session->set_flashdata('success', lang('success'));
				redirect('admin/shop/product/edit/'.$product_id);
			} 
			else 
			{
				$this->session->set_flashdata('error', lang('create_product_error') ); //
				redirect('admin/shop/product/create');
			}
		}
		else
		{
			if( $this->input->post())
			{					
				foreach ($this->item_validation_rules AS $rule)
					$this->data->{$rule['field']} = $this->input->post($rule['field']);
			}

		}
		
		


		
		// Build the Template
		$this->template->title($this->module_details['name'], lang('shop:common:create'))
				->append_metadata($this->load->view('fragments/wysiwyg', $this->data, TRUE))
				->append_js('module::admin/product.js')
				->build('admin/products/create', $this->data);
	}


	
	
	/**
	 * Edit a product
	 *
	 *
	 */
	public function edit( $id = 0 ) 
	{


		// Determine whether to get by id or slug
		$method = (is_numeric($id))  ? 'id' : 'slug' ;

		

		// Get the product and all its goodness
		$data = $this->products_admin_m->get($id, $method);



		if(!$data )
		{
			$this->session->set_flashdata('notice',lang('shop:messages:no_product_found') );
			redirect('admin/shop/products/');
		}


		//
		// Run validation if postback
		//
		if ($this->form_validation->run()) 
		{
			
			$input = $this->input->post();
		
		
			// save
			if ($this->products_admin_m->edit($data->id, $input)) 
			{	

				Events::trigger('evt_product_changed', $data->id);
				
				$this->session->set_flashdata('success', lang('success'));
				
			} 
			else 
			{
				$this->session->set_flashdata('error', lang('error'));
			}
			

			if(isset($input['btnAction']))
			{
				if($input['btnAction'] == 'save_exit')	redirect('admin/shop/products/');
			}
			
			
			redirect('admin/shop/product/edit/' . $data->id);

			
		}


		$data->modules = $this->getInstalledModules();


		$this->load->library('design_library');
		$_path = Settings::get('default_theme');
		$data->design_select 	= $this->design_library->build_list_select( $_path , array('current_id' => $data->page_design_layout) );	
		

		$this->load->library('products_library');
		$data->req_shipping_select = $this->products_library->build_requires_shipping_select(array('current_id' => $data->req_shipping));		

		// Build Template
		$this->template->title($this->module_details['name'], lang('shop:common:edit'))
				->append_metadata($this->load->view('fragments/wysiwyg', $data, TRUE))
				->append_js('jquery/jquery.tagsinput.js')
				->append_js('module::admin/product.js')
				->append_css('jquery/jquery.tagsinput.css')
				->build('admin/products/form', $data);

	}
	
	
		
	
	
	/**
	 * This is used to duplicate an existing item in the products database to make selling online faster and easier
	 * Once the product has been duplicated - It will automaticly set itself to "Invisible"
	 * It will require the admin to manually set this to visible. This is a safeguard so that we dont just
	 * duplicate an item and its available online straight away..
	 *
	 *
	 * @param INT $id The ID of the product to duplicate
	 * @access public
	 */
	public function duplicate( $id = 0 ) 
	{
		// Set the default redirect page
		$redir = 'admin/shop/products';
	
		if (is_numeric($id))
		{

			$product_id = $this->products_admin_m->duplicate($id);
			
			if ($product_id) 
			{
				Events::trigger('evt_product_created', $product_id);
				
				$this->session->set_flashdata('success', lang('success'));
				
				$redir = 'admin/shop/product/edit/'.$product_id;
				
			} 
			else 
			{
				$this->session->set_flashdata('error', lang('error'));
			}
			
		}
		
		redirect($redir);
		
	}



	/**
	 * Delete the Item -set delete flag to 0 
	 * @param INT $id
	 */
	public function delete($id = 0)
	{

		if (is_numeric($id)) 
		{
			$result = $this->products_admin_m->delete($id);
			if ($result)
				Events::trigger('evt_product_deleted', $id);

		}

		redirect('admin/shop/products');
	}
	
	



	public function load( $id, $panel = '' ) 
	{

		//List of all SHOP modules compatible with core
		$array_of_modules = array(
			'attributes' 	=> 'shop_attributes',
			'options' 		=> 'shop_options',
			'categories' 	=> 'shop_categories',
			'related' 		=> 'shop_related',
			'digitalfiles' 	=> 'shop_digitalfiles',
			'images' 		=> 'shop_images',
			'seo' 			=> 'shop_seo',
			'inventory' 	=> 'shop_inventory'
		);



		$namespace = $array_of_modules[$panel];


		//Build location of view based on req
		$view_file = $namespace . '/admin/products/partials/'.$panel;
		$Integration_library = $namespace . '/Integration_library';


		$data = $product =  $this->products_admin_m->get($id);

		$this->load->library($Integration_library);

		$tab_data = $panel . '_data';
		
		$product->$tab_data = $this->Integration_library->get_tab($product);

		$this->load->view($view_file, $product); 



		//Each needs an Integration librry
		/*
		if($panel =='brand')
		{
			$data->brand_select 	= $this->brands_m->build_dropdown($data->brand_id);
		}	
		if($panel =='images')
		{
			$this->load->model('shop_images/images_m');
			$data->images 			= $this->images_m->get_images($data->id);  
			$data->folders = $this->get_folders();
			$view_file = 'shop_images/admin/products/partials/'.$panel;
		}
		if($panel =='attributes')
		{
			$this->load->model('shop_attributes/product_attributes_m');
			$data->properties_array = $this->product_attributes_m->get_by_product($data->id);  
			$view_file = 'shop_attributes/admin/products/partials/'.$panel;
		}
		if($panel =='categories')
		{
			$this->load->model('shop_categories/categories_m');
			$data->category_select 	= $this->categories_m->build_dropdown( array( 'field_property_id' => 'category_id', 'current_id' => $data->category_id ) );
			$view_file = 'shop_categories/admin/products/partials/'.$panel;
		}
		if($panel == 'options')
		{  
			$view_file = 'shop_options/admin/products/partials/'.$panel;
		}		
		if($panel == 'seo')
		{  
			$view_file = 'shop_seo/admin/products/partials/'.$panel;
		}	
		if($panel == 'inventory')
		{  
			$view_file = 'shop_inventory/admin/products/partials/'.$panel;
		}				
		if($panel =='related')
		{

			$data->related = json_decode($data->related);

			$data->rel_names = array();
			
			foreach($data->related as $related_product)
			{
				$data->rel_names[] = $this->products_admin_m->get($related_product);
			}	
			$view_file = 'shop_related/admin/products/partials/'.$panel;

		}		
		if($panel == 'files')
		{
			$this->load->model('shop/shop_files_m');
			$data->digital_files = $this->shop_files_m->get_files($data->id);
			$view_file = 'shop_digitalfiles/admin/products/partials/'.$panel;
		}			


		$this->load->view($view_file, $data); 
		*/
	}



	public function visibility() 
	{

		// Get the post from the client
		$input = $this->input->post();

		$id = $input['id'];
		$current_state = $input['action'];
		$input['message'] = '';



		switch( $current_state ) 
		{
			case ProductVisibility::Invisible:
				$status	= 'Stop';
				$input['changeto'] = ProductVisibility::Visible;
				break;
			
			case ProductVisibility::Visible:
				$status	= 'Sell';
				$input['changeto'] = ProductVisibility::Invisible;
				break;
		}



		
		if(  $this->products_admin_m->update_property( $id , 'public', intval( $input['changeto'] ) ) )	
		{
			$input['status'] = $status;
		}
		else
		{
			$input['status'] = JSONStatus::Error;
		}

		
		Events::trigger('evt_product_changed', $id ); 
		
		
		echo json_encode($input);die;
	
	}	



	/**
	 *
	 * @return Array of images for gallery selection
	 */
	protected function get_folders() 
	{
		
		// Load Files Module
		$this->load->library('files/files');
		$this->load->model('files/file_folders_m');
		
		// Set up the Dropdown Array for edit and create
		$folders = array(0 => lang('global:select-pick'));
		
		// Get All folders
		$tree = $this->file_folders_m->order_by('parent_id', 'ASC')->order_by('id', 'ASC')->get_all();
		
		// Build the Folder Tree
		foreach ($tree as $folder) 
		{
			$id = $folder->id;
			if ($folder->parent_id != 0) 
			{
				$folders[$id] =  $folders[$folder->parent_id] . ' &raquo; ' . $folder->name;
			} 
			else
			{
				$folders[$id] = $folder->name;
			}
		}
		
		return $folders;
	}	

}
