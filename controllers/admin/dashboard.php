<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Dashboard extends Admin_Controller 
{
	// Set the section in the UI - Selected Menu
	protected $section = 'dashboard';

	public function __construct() 
	{
		parent::__construct();

		$this->data = new stdClass();

		// Load all the Required classes
		$this->load->model('shop/orders_m');

		$this->template
				->append_js('module::maps_bing.js')
				->append_css('module::admin.css');
	}


	/**
	 * List all items:load the dashboard
	 */
	public function index() 
	{

		
		// Load required Classes
		$this->load->model('shop/products_admin_m');
		$this->load->model('shop_categories/categories_m','categories_m');
		$this->load->model('shop_analytics/statistics_m');


		// No pagination on dashboard for **recent orders**. On orders page we will have them
		$max = Settings::get('nc_total_recent_orders');
		
		// bing maps api - we need to pass the api key
		$this->data->MapAPIKey =Settings::get('shop_maps_api_key');
		
		// Could do withy some caching here
		$this->data->shop_products_count = $this->statistics_m->get_catalogue_data();
		
		// Collect all orders
		$this->data->order_items = $this->orders_m->order_by('order_date','desc')->limit($max)->get_all();
		
	
		
		
		//TODO: Use better chart api, also improve the data
		$rows = $this->db->query("select order_date, count(id) as `Val` from ".$this->db->dbprefix('shop_orders')." group by DATE(FROM_UNIXTIME(order_date)) LIMIT 4");
		$this->data->SalesRecords = $rows->result();
		

		// Build the view with shop/views/admin/items.php
		//$data->items = & $items;
		$this->template->title($this->module_details['name'])
				->append_metadata('<script src="http://www.google.com/jsapi"></script>')
				->append_js('module::admin/dashboard.js')
				->append_js('jquery/jquery.flot.js')
				->build('admin/dashboard/dashboard', $this->data);
				
	}


    
}
