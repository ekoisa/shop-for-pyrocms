<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Shop extends Public_Controller 
{
	private $data;

	public function __construct() 
	{
		parent::__construct();
		$this->data = new stdClass();
	}


	/**
	 * To add a custom home page for the domain.com/shop name space add a layout to your theme.
	 * Call it shop_home.html
	 *
	 * Then create a page in your pages module called shop.
	 *
	 * In the content/description area you can add widgets or plugins to display whatever content you like.
	 * 
	 * @param  string $param [description]
	 * @return [type]        [description]
	 */
	public function index($param = '') 	
	{

		if($this->template->layout_exists('shop_home.html'))
		{
			$this->template->set_layout('shop_home.html'); 
		}
		elseif($this->template->layout_exists('shop.html'))
		{
			$this->template->set_layout('shop.html');
		}

		$this->template->build('common/home', $this->data);
	}

	

	public function closed($param = '') 	
	{
		$this->open_shop 	= Settings::get('nc_open_status');  /* shop open closed */

		if($this->open_shop)
		{
			redirect('shop');
		}
		else
		{
			//display closed shop here
		}
	}



	/**
	 * The shop login function
	 *
	 * Point the login link to here site/shop/login and after you login you will be taken to wherever you came from.
	 * 
	 * @return [type] [description]
	 */
	public function login()
	{

		// Get the page where we came from
		$url_redir = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'shop';

		// Set the magic session value to redirect back
		$this->session->set_userdata('shop_force_redirect' , $url_redir );

		// Now go to login page
		redirect('users/login');
	}


}