<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>

<div id="sortable">

		
	<div class="one_full" id="">

		<section class="title">
			<h4><?php echo lang('shop:manage:shop_settings'); ?></h4>
			<a class="" title=""></a>
		</section>
		<section class="item">
			<div class="content">
				<div class="tabs">
					<ul class="tab-menu">
						<li><a href="#setup-tab-1"><?php echo lang('shop:manage:files'); ?></a></li>	
						<li><a href="#setup-tab-2">Appearace</a></li>			
					</ul>
					<div id="setup-tab-1" class="form_inputs">
						
						<fieldset>	
							<div class="content">
								<table>
									<tr>
										<th><?php echo lang('shop:manage:detail');?></th>
										<th><?php echo lang('shop:common:value');?></th>
									</tr>

									<tr>
										<td><?php echo lang('shop:manage:upload_order');?></td>
										<td>
											<span style='float:right'><?php echo $shop_upload_file_orders;?></span>
										</td>											
									</tr>

									<tr>
										<td><?php echo lang('shop:manage:upload_products');?></td>
										<td>
											<span style='float:right'><?php echo $shop_upload_file_product;?></span>
										</td>										
									</tr>
									<tr>
										<td>Folder to Upload Digital products</td>
										<td>
											<span style='float:right'><?php echo $shop_digital_product_location;?></span>
										</td>										
									</tr>									
								</table>
							</div>
						</fieldset>
						
					</div>				

					<div id="setup-tab-2" class="form_inputs">
						<fieldset>
							<div class="content">
								<table>
									<tr>
										<th><?php echo lang('shop:manage:detail');?></th>
										<th><?php echo lang('shop:common:value');?></th>
									</tr>

									<tr>
										
											<td><?php echo lang('shop:manage:login_location');?></td>
											<td>
												<span style='float:right'><?php echo $shop_admin_login_location;?></span>
											</td>
										
									</tr>
								</table>
							</div>
						</fieldset>
					</div>	


				</div>


						


						
				

				<div class="buttons">
					<button class="btn blue" value="save_exit" name="btnAction" type="submit">
						<span><?php echo lang('shop:products:save_and_exit');?></span>
					</button>	
					<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
				</div>	


					


			</div>


		</section>


	
	</div>
	
	
</div>	
<?php echo form_close(); ?>

<script type="text/javascript">



			

</script>



