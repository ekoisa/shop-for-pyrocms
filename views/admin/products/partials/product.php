

				<fieldset>
					<ul>
						<?php if ($id == null): ?>
						<?php else: ?>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="id"><?php echo lang('shop:products:true_id'); ?> <span>*</span></label>
							<div class="input"><?php echo $id; ?></div>
						</li>			
						<?php endif; ?>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="name"><?php echo lang('shop:common:name'); ?> <span>*</span>
								<small>
									<?php echo lang('shop:products:name_description'); ?>
								</small>
							</label>
							<div class="input" ><?php echo form_input('name', set_value('name', $name)); ?></div>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="slug"><?php echo lang('shop:common:slug'); ?> <span>*</span>
								<small>
									<?php echo lang('shop:products:slug_description'); ?>
								</small>
							</label>
							<div class="input"><?php echo form_input('slug', set_value('slug', $slug)); ?></div>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="code"><?php echo lang('shop:products:code'); ?> <span></span>
							<small>
							 <?php echo lang('shop:products:code_description'); ?>
							</small>
							</label>
							<div class="input"><?php echo form_input('code', set_value('code', $code)); ?></div>
						</li>					
						

						<?php /* Check if brands is installed */if (Settings::get('ss_enable_brands') == 1) :?>

						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="brand_id"><?php echo lang('shop:products:brand'); ?></label>
							<div class="input">
								<?php echo $brand_select; ?> 
							</div>
						</li>	

						<?php endif; ?>
						
						

			
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="brand_id"><?php echo lang('shop:products:featured'); ?><span></span>
								<small>
								<?php echo lang('shop:products:featured_description'); ?>
								</small>
							</label>
							<div class="input">
							<?php
									echo form_dropdown('featured', array(
										1 => lang('shop:common:yes'), 
										0 => lang('shop:common:no'), 
										), set_value('featured', $featured));
									?>
							</div>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="brand_id"><?php echo lang('shop:products:searchable'); ?><span></span>
								<small>
								 <?php echo lang('shop:products:searchable_description'); ?>
								</small>
							</label>
							<div class="input">
							<?php
									echo form_dropdown('searchable', array(
										1 => lang('shop:common:yes'), 
										0 => lang('shop:common:no'), 
										), set_value('searchable', $searchable));
									?>								
							</div>
						</li>
				
					</ul>
				</fieldset>

			<fieldset>

				<label>
					
					<?php echo lang('shop:products:price_tab_description'); ?>

					<small><?php echo lang('shop:products:price_tab_description_description'); ?></small>

				</label>



				<div class="tabs">		

					<ul class="tab-menu">
						<li><a class=""  data-load="" href="#price-basic-tab"><span><?php echo lang('shop:products:standard'); ?></span></a></li>		
					</ul>	


					<div class="form_inputs" id="price-basic-tab">
						<fieldset>
								<ul>
									<li>
										<label for="price"><?php echo lang('shop:common:price'); ?><span>*</span><br />
											<small><?php echo lang('shop:products:price_description'); ?></small>
										</label>
										<div class="input">
											<?php echo ss_currency_symbol().' '.form_input('price', set_value('price', $price), "placeholder='0.00'"); ?>
										</div>
									</li>
				
									<li>
										<label for="price"><?php echo lang('shop:products:rrp'); ?> <span></span><br />
											<small><?php echo lang('shop:products:rrp_description'); ?></small>
										</label>
										<div class="input">
											<?php echo ss_currency_symbol().' '.form_input('rrp', set_value('rrp', $rrp), "placeholder='0.00'"); ?>
										</div>
									</li>	
													
								</ul>

						</fieldset>
					</div>



				</div>


			</fieldset>					


				<fieldset>	
					<ul>
						<li class="<?php echo alternator('', 'even'); ?>">
							<label for=""><?php echo lang('shop:common:description'); ?><span></span>
								<small>
								 <?php echo lang('shop:products:description_description'); ?>
								</small>
							</label>
						</li>
						<li class="<?php echo alternator('', 'even'); ?>">
							<div class="">
								<?php echo form_textarea('description', set_value('description', $description), 'class="wysiwyg-simple"'); ?> 
							</div>
						</li>					
					</ul>			
					   
					
				</fieldset>


				<fieldset>
					<ul>
						<li>
							<label>
								<?php echo lang('shop:products:page_design'); ?>
								<small>
									<?php echo lang('shop:products:page_design_description'); ?>
								</small>
							</label>

							<div class="input">
								<select name="page_design_layout" id="page_design_layout">
									<option value="products_single"><?php echo lang('shop:products:default'); ?></option>
									<?php echo $design_select; ?> 
								</select>
							</div>
						</li>	

					</ul>
				</fieldset>			




				<fieldset>
					<ul>
						<li>
							<label>
								<?php echo lang('shop:common:shipping'); ?>
								<small>
									<?php echo lang('shop:products:shipping_description'); ?>
								</small>
							</label>
							<div class="input">
							</div>
						</li>	


						<li class="<?php echo alternator('', 'even'); ?>">

									<label for="brand_id"><?php echo lang('shop:products:req_shipping'); ?> <span>*</span>
										<small>
											<?php echo lang('shop:products:req_shipping_description'); ?>
										</small>
									</label>
									<div class="input">
										<select name="req_shipping" id="req_shipping">
											<option value=""><?php echo lang('global:select-pick'); ?></option>
											<?php echo $req_shipping_select; ?> 
										</select>
									</div>
						</li>	


						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="height"><?php echo lang('shop:products:height'); ?> 
								<span></span>
								<small>
								 	<?php echo lang('shop:products:height_description'); ?>
								</small>
							</label>
							<div class="input"><?php echo form_input('height', set_value('height', $height)); ?></div>
						</li>	

						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="width"><?php echo lang('shop:products:width'); ?> 
								<span></span>
								<small>
								 	<?php echo lang('shop:products:width_description'); ?>
								</small>
							</label>
							<div class="input"><?php echo form_input('width', set_value('width', $width)); ?></div>
						</li>	


						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="depth"><?php echo lang('shop:products:depth'); ?> 
								<span></span>
								<small>
								 	<?php echo lang('shop:products:depth_description'); ?>
								</small>
							</label>
							<div class="input"><?php echo form_input('depth', set_value('depth', $depth)); ?></div>
						</li>	


						<li class="<?php echo alternator('', 'even'); ?>">
							<label for="weight"><?php echo lang('shop:products:weight'); ?> 
								<span></span>
								<small>
								 	<?php echo lang('shop:products:weight_description'); ?>
								</small>
							</label>
							<div class="input"><?php echo form_input('weight', set_value('weight', $weight)); ?></div>
						</li>	

					</ul>
				</fieldset>
								


<?php
 
/* SCRIPT SECTION
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 */
 ?>


<script type="text/javascript">

	var instance;

	function update_instance()
	{
		instance = CKEDITOR.currentInstance;
	}

	(function($) {
		$(function(){

			pyro.init_ckeditor = function(){
				<?php echo $this->parser->parse_string(Settings::get('ckeditor_config'), $this, TRUE); ?>
				pyro.init_ckeditor_maximize();
			};
			pyro.init_ckeditor();

		});
	})(jQuery);

</script>
			

			<script>

				pyro.generate_slug('input[name="name"]', 'input[name="slug"]');

				/**
				* 
				* @param  {[type]} e [description]
				* @return {[type]}   [description]
				*/
				$('input[name="name"]').live('change', function(e) 
				{

					var new_name = $(this).val();

					$("#title_product_name").html(new_name);

					return false;
				}); 

			</script>