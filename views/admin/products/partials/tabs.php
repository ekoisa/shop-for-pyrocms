

				<li><a class="tab-loader"  data-load="product" href="#product-tab"><span><?php echo lang('shop:common:product'); ?></span></a></li>

				
				<?php if($modules['shop_images'] ==1): ?>
					<li><a class="tab-loader"  data-load="images" href="#images-tab"><span><?php echo lang('shop:common:images'); ?></span></a></li>
				<?php endif; ?>


				<?php if($modules['shop_attributes'] ==1): ?>
					<li><a class='tab-loader'  data-load='attributes' href='#attributes-tab'><span><?php echo lang('shop:products:attributes'); ?></span></a></li>
				<?php endif; ?>
				

				<?php if($modules['shop_categories'] ==1): ?>
					<li><a class='tab-loader'  data-load='categories' href='#categories-tab'><span><?php echo lang('shop:products:categories'); ?></span></a></li>
				<?php endif; ?>


				<?php if($modules['shop_options'] ==1): ?>
					<li><a class="tab-loader"  data-load="options" href="#options-tab"><span><?php echo lang('shop:products:options'); ?></span></a></li>
				<?php endif; ?>


				<?php if($modules['shop_related'] ==1): ?>
					<li><a class="tab-loader"  data-load="related" href="#related-tab"><span><?php echo lang('shop:products:related_products'); ?></span></a></li>
				<?php endif; ?>
				

				<?php if($modules['shop_digitalfiles'] ==1): ?>
					<li><a class="tab-loader"  data-load="files" href="#files-tab"><span><?php echo lang('shop:products:files'); ?></span></a></li>
				<?php endif; ?>


				<?php if($modules['shop_seo'] ==1): ?>
					<li><a class="tab-loader"  data-load="seo" href="#seo-tab"><span><?php echo lang('shop:products:seo'); ?></span></a></li>
				<?php endif; ?>


				<?php if($modules['shop_inventory'] ==1): ?>
					<li><a class="tab-loader"  data-load="inventory" href="#inventory-tab"><span><?php echo lang('shop:products:inventory'); ?></span></a></li>
				<?php endif; ?>