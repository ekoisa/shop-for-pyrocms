<div id="customer-portal">
	
	<h2>{{ helper:lang line="shop:my:order" }}</h2>

	<ul class="my-links clearfix">
		{{ shop:mylinks remove='shop messages' active='orders' }}
                    <li>{{link}}</li>
		{{ /shop:mylinks }}	
	</ul>


	{{ if order.pmt_status == 'unpaid' }}
		<a href="{{ url:site }}shop/payment/order/{{order.id}}">Pay now</a>
	{{ endif }}


	<h4>
			{{ helper:lang line="shop:my:order_details" }}
	</h4>

	<!--  ORDER DETAILS  -->
	<table style="width: 100%">
		<tr>
			<td>
				<div><?php echo lang('shop:label:order_id'); ?> : {{order.id}}</div>
				<div><?php echo lang('shop:label:date'); ?> : {{helper:date format="d-M-Y" timestamp=order.order_date}}</div>
			</td>
			<td>
				<div><?php echo lang('shop:label:order_status'); ?> : {{order.status}}</div> 
				<div><?php echo lang('shop:label:payment_status'); ?> : {{order.pmt_status}}</div> 
				<div><?php echo lang('shop:label:shipping_method'); ?> : {{order.shipping_id}}</div> 
			</td>
		</tr>
	</table>
	
	<hr />
	
	<!--  ORDER ADDRESS  -->
	<table style="width: 100%">
		<tr>
			<th>
				{{ helper:lang line="shop:label:billing_address" }}
			</th>
			<th>
				{{ helper:lang line="shop:label:shipping_address" }}
			</th>
		</tr>
		<tr>
				<td>

					{{invoice.first_name}} {{invoice.last_name}}<br />
					{{invoice.company}}<br /> 
					{{invoice.address1}} {{invoice.address2}}<br />
					{{invoice.city}} {{invoice.zip}}<br />
					{{if invoice.email}} 
						<a href='mailto:{{invoice.email}}'>
							{{invoice.email}}
						</a><br />
					{{endif}}
				</td>
				<td>
					{{shipping.first_name}} {{shipping.last_name}}<br />
					{{shipping.company}}<br /> 
					{{shipping.address1}} {{shipping.address2}}<br />
					{{shipping.city}} {{shipping.zip}}<br />
					{{if shipping.email}} 
						<a href='mailto:{{shipping.email}}'>
							{{shipping.email}}
						</a><br />
					{{endif}}
				</td>
		</tr>
	</table>
		
	<h4><?php echo lang('shop:my:order_items'); ?></h4>
	

	<table style="width: 100%">
		<thead>
			<tr>
				<th><?php echo lang('shop:label:image'); ?></th>
                <th><?php echo lang('shop:label:qty'); ?></th>
				<th><?php echo lang('shop:label:name'); ?></th>
				<th><?php echo lang('shop:label:price'); ?></th>
				<th><?php echo lang('shop:label:action'); ?></th>
			</tr>
		</thead>
		<tbody>
		
			{{contents}}
				<tr>
					<td>
                        {{shop:product_cover id="{{id}}"}} 
                                <img src="{{ src }}" alt="{{ alt }}"  height="100" />  
						{{/shop:product_cover}}
					</td>
					<td>{{qty}}</td>
					<td>{{title}}</td>
					<td>{{shop:currency}} {{cost_item}}</td>
					<td><a href="{{ url:site }}shop/products/product/{{ product_id }}">view</a></td>
				</tr>



			{{/contents}}

		</tbody>



	</table>


	{{shop:digital_files order_id="{{order.id}}"  order_status="{{order.pmt_status}}" }}
		
			{{if count > 0}}
				<table>
				<h2>Downloads</h2>
					
					{{filelist}}
						<tr>
							<td></td>
							<td>{{id}}</td>
							<td>{{filename}}</td>
							<td>
								{{if order_status =='paid'}}
									<a href='{{url}}'><?php echo lang('shop:label:download'); ?></a>
								{{endif}}
							</td>
						</tr>
					{{/filelist}}

				</table>
			{{endif}}

	{{/shop:digital_files}}

			

	

	
	<h4>Order Messages</h4>
	
	<table>
		<thead>			
			<tr>
				<td>Date</td>
				<td>Message</td>
			</tr>
		</thead>
		<tbody>
			{{messages}}
				<tr>
					<td>{{date_sent}}</td>
					<td>{{message}}</td>
				</tr>
			{{/messages}}
		</tbody>
		<tfoot>
			<?php echo form_open(); ?>
			<tr>
				<td>
					Message
				</td>
				<td colspan="2">
					<?php echo form_textarea(array(
						'name' => 'message',
						'value' => set_value('message'),
						'rows' => 3
					)); ?><br />
					<?php echo form_submit('save', 'Send'); ?>
				</td>
			</tr>
			<?php echo form_close(); ?>
		</tfoot>
	</table>

</div>