<div id="">

	<h2><?php echo lang('shop:my:orders'); ?></h2>


	<ul class="my-links clearfix">
		{{ shop:mylinks remove='shop messages' active='orders' }}
                    <li>{{link}}</li>
		{{ /shop:mylinks }}	
	</ul>


	<div class="my-orders">

			<table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Date</th>

						<th>Total</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

	
					{{items}}
					<tr>
						<td># {{id}}</td>
						<td>{{helper:date format="d-M-Y" timestamp=order_date}}</td>

						<td>{{shop:currency}} {{cost_total}}</td>
						<td>{{status}}</td>
						<td>
								<a href="{{ url:site }}shop/my/orders/order/{{id}}" class="">view</a>

								{{shop:order_is_unpaid id="{{id}}" }}
									<a href="{{ url:site }}shop/payment/order/{{id}}" class="">pay now</a>
								{{/shop:order_is_unpaid}}

								{{shop:order_is_paid id="{{id}}" }}
									Thank you for your payment
								{{/shop:order_is_paid}}
						 </td>

					</tr>
					{{/items}}

				</tbody>
				
			</table>

	</div>
</div>