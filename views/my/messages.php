<div>
<h2><?php echo lang('shop:my:messages'); ?></h2>

	<ul class="my-links clearfix">
		{{ shop:mylinks remove='shop' active='messages' }}
                    <li>{{link}}</li>
		{{ /shop:mylinks }}	
	</ul>


        {{ if messages }}
		<div class="my-messages">
			<table>
				<thead>
					<tr>
						<td><?php echo lang('shop:label:id'); ?></td>
						<td><?php echo lang('shop:label:user_id'); ?></td>
						<td><?php echo lang('shop:label:order_id'); ?></td>
						<td><?php echo lang('shop:label:message'); ?></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					{{messages}}
					<tr>
						<td>{{id}}</td>
						<td>{{order_id}}</td>
						<td>{{message}}</td>
						<td>{{user_id}}</td>
						<td><a href="{{ url:site }}shop/my/orders/order/{{order_id}}" class="button">View order</a></td>
					</tr>
					{{/messages}}
				</tbody>
			</table>

                </div>
                {{ else }}
                    <h4>There are no messages for you.</h4>
                {{ endif }}                        
		<p>
			<a href="{{ url:site }}shop/my" class="button"><?php echo lang('shop:label:back_to_dashboard'); ?></a>
		</p>
	
</div>
