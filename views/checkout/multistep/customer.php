

<form name="" method="post" action="{{url:site}}shop/checkout/">
    <fieldset>
        <p>Continue as guest or register a user account</p><br />

        <ul>

            {{ shop:settings }}

                {{if allow_guest==true}}
                    <li class="<?php echo alternator('odd', 'even'); ?>">
                        <div class="input">
                            <label><?php echo form_radio('customer', 'guest', set_radio('customer', 'guest')); ?> {{ helper:lang line="shop:messages:checkout:continue_as_guest" }}</label>
                        </div>
                    </li>
                {{else}}

                {{endif}}


            {{ /shop:settings }}


            <li class="<?php echo alternator('odd', 'even'); ?>">
                <div class="input">
                    <label>
                        <input type='radio' name='customer' value='register'>
                        {{ helper:lang line="shop:messages:checkout:register_as_new_user" }}
                    </label>
                </div>
            </li>

        </ul>

        <div class="buttons"> 
            <a class="shopbutton" href='{{url:site}}shop/cart'>{{ helper:lang line="shop:messages:checkout:back_to_cart" }}</a> or <input class="shopbutton"type='submit' name='submit' value='continue'>
        </div>       

    </fieldset>
</form>

<hr />


<fieldset>

    <p>Or if you a returning customer, login here</p> <br />


    <form name="logincheckout" method="post" action="{{url:site}}users/login">

        <input type='hidden' name='redirect_to' value='shop/checkout'>

        <ul>
            <li>
                <label>email</label>
                <div class="input">
                    <input type='text' name='email' value='' style='' id="email" maxlength="120">
                </div>
            </li>
            <li>
                <label>password</label>
                <div class="input">
                    <input type='password' name='password' value='' style='' id="password" maxlength="20">
                </div>
            </li>
            <li>
                <label>
                    <input type='checkbox' name='remember' value='1'>
                    Remember Me
                </label>
            </li>

            <li class="links">
                <span id="reset_pass">
                     <a href='{{url:site}}users/reset_pass'>forgot your password ?</a> 
                </span>
            </li>

        </ul>



        <div class="login-buttons">
            <button type="submit" class="login_submit">
                Login
            </button>
        </div>  
    </form>
</fieldset>
