<?php
    if (validation_errors()) 
        {
             echo "<div class='errors'>" . validation_errors() . "</div>";
        }
?>
{{if addresses}}
    <h2>Shipping address</h2>

            <form name="form1" action="{{url:site}}shop/checkout/shipping/" method="POST">
                <input type='hidden' value='existing' name='selection'>
                <fieldset>
                    <h3>Select an existing address</h3>
                    <table>
                        {{addresses}}
                            <tr>
                                <td>
                                    <input type="radio" name="address_id" value="{{id}}">
                                </td> 
                                <td>
                                    {{address1}}, {{address2}}  
                                </td>  
                                <td>
                                    {{city}} {{country}}   
                                </td>  
                                <td>
                                    {{state}} {{zip}}   
                                </td>                                                                         
                            </tr>
                         {{/addresses}}                           
                    </table>
            </fieldset>
            <fieldset>
                
                <div  class="buttons"> 
                    <a class="shopbutton" href='{{url:site}}shop/cart'>{{ helper:lang line="shop:messages:checkout:back_to_cart" }}</a> or <input class="shopbutton"type='submit' name='submit' value='continue'>
                </div>

            </fieldset>
           </form>
           <hr />
        {{endif}}


<form name="form2" action="{{url:site}}shop/checkout/shipping/" method="POST">
    <input type='hidden' value='new' name='selection'>
    <fieldset>
        <h2>New Address</h2>

        <ul class="two_column">
            <li>
                <label>First name <span class="required">*</span></label>
                <div class="input">
                    <input type="text" name="first_name" value="{{first_name}}">
                </div>
            </li>
            <li>
                <label>Last Name <span class="required">*</span></label>
                <div class="input">
                    <input type="text" name="last_name" value="{{last_name}}">
                </div>
            </li>
            <li>
                <label>Email <span class="required">*</span></label>
                <div class="input">
                     <input type="text" name="email" value="{{email}}">
                </div>
            </li>
            <li>
                <label>Company <span class="required">*</span></label>
                <div class="input">
                     <input type="text" name="company" value="{{company}}">
                </div>
            </li>            
            <li>
                <label>Phone <span class="required">*</span></label>
                <div class="input">
                    <input type="text" name="phone" value="{{phone}}">
                </div>
            </li>
            <li>
                <label>address1 <span class="required">*</span></label>
                <div class="input">
                    <input type="text" name="address1" value="{{address1}}">
                </div>
            </li>
            <li>
                <label>address2</label>
                <div class="input">
                     <input type="text" name="address2" value="{{address2}}">
                </div>
            </li>
            <li>
                <label>City <span class="required">*</span></label>
                <div class="input">
                    <input type="text" name="city" value="{{city}}">
                </div>
            </li>
            <li>
                <label>State</label>
                <div class="input">
                    <input type="text" name="state" value="{{state}}">
                </div>
            </li>
            <li>
                <label>Country</label>
                <div class="input">
                    <select name='country'>
                        {{countries}}
                            <option value='{{code}}'>{{name}}</option>
                        {{/countries}}
                    </select>
                </div>
            </li>
            <li>
                <label>ZIP/Postcode <span class="required">*</span></label>
                <div class="input">
                    <input type="text" name="zip" value="{{zip}}">
                </div>
            </li>                        
        </ul>
   


    </fieldset>


    <fieldset>

        <div class="buttons"> 
            <a class="shopbutton" href='{{url:site}}shop/cart'>back to cart</a> or <input class="shopbutton"type='submit' name='submit' value='continue'>
        </div>

    </fieldset>

</form>