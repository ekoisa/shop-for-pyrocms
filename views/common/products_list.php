<div id="MultipleItemListView">

        <div class="product-list">

            {{ if category_id != "0" }}
                
                {{shop:category id="{{category_id}}" }}
                    <h2>{{name}}</h2>
                {{/shop:category}}

            {{ endif }}
<!-- If there are no products for this category show message -->
            {{if product_count==0}}
                <h3>{{ helper:lang line="shop:messages:product:no_products" }}</h3>
            {{else}}

                {{ shop:products limit="{{limit}}" offset="{{offset}}" category_id="{{category_id}}" }}


                    {{ if searchable != "1" }}
                        {{# Do not list searchable items in main list #}}
                        A product is found to be non searchable
                    {{ else }}

                        <div itemscope itemtype="http://schema.org/Product" id="ProductItem">

                                <div itemprop="name" style="display:none;">
                                    {{ name }}
                                </div>

                                <a itemprop="url" href="{{ url:site }}shop/products/product/{{ slug }}">


                                    {{shop_images:images id="{{id}}" include_cover='YES' include_gallery='NO' }}

                                            {{if local}}
                                                <img itemprop="image" src="{{ url:site }}files/thumb/{{file_id}}/200/200/" width="200" height="200" alt="{{alt}}" />
                                            {{else}}
                                                <img itemprop="image" src="{{src}}" width="200" height="200" alt="{{alt}}" />
                                            {{endif}}

                                    {{/shop_images:images}}

                                </a>

                                <h4 itemprop="name">{{name}}</h4>
                                <div class="item-description"> {{ meta_desc }} </div>
                                <div class="item-price">
                                    {{shop:price id='{{id}}'}} 
                                        {{if min_qty == 1}}                                                                                
                                            Price ea: ${{ price }} 
                                        {{endif}}
                                    {{/shop:price}}                                                                                
                                </div>


                                <form action="{{url:site}}shop/cart/add" name="form_{{ id }}" method="post">

                                    <input type="hidden" name="id" value="{{ id }}">

                                    {{ if status != "in_stock" }}  <!-- how does status not equal "in_stock" please ? -->
                                            <a class="shopbutton" href="{{ url:site }}shop/products/product/{{ slug }}">view</a>
                                    {{ else }}


                                            <ul class="clearfix">
                                                <li><label class="qty-label">Quantity</label></li>
                                                <li><input class="qty-enter" name="quantity" data-max="0" data-min="" maxlength="5" title="Qty" value="1" /></li>
                                                <li><input type="submit" value='add to cart' class="shopbutton" id="{{id}}"  /></li>  <!-- is the id needed ? -->
                                            </ul>


                                    {{ endif }}

                                </form> 

                        </div>

                    {{endif}}

                {{ /shop:products }}
            {{ endif }} 
        </div>


        {{ if pagination:links }} 
            <div class="pagination"> 
                {{ pagination:links }}
            </div>
        {{ endif}} 


</div>