<div id="CartView">
    <div id="cart-title">
        <h2>Your cart</h2>
    </div> 

    
    {{ shop:cart }}

        {{if item_count > 0}}

            <form action="{{url:site}}shop/cart/update" method="POST">
                <!-- Start Shopping Cart Table -->
                <table class="cart-table">
                    <thead>
                        <tr>
                            <th class="image">item</th>
                            <th class="description">Description</th>
                            <th class="price">Price</th>
                            <th class="qty">Quantity</th>
                            <th class="subtotal">Subtotal</th>
                            <th class="remove">Remove</th>				
                        </tr>
                    </thead>
                    <tbody>
                        {{contents}}
                		<tr>
                		    <input type="hidden" name="{{rowid}}[rowid]" value="{{rowid}}">
                                    <input type="hidden" name="{{rowid}}[id]" value="{{id}}">
                        	    <td class="image">
                        		    {{shop:images id="{{ id }}" include_cover='YES' include_gallery='NO' }}
                                                {{if local}}
                                                    <img itemprop="image" src="{{ url:site }}files/thumb/{{file_id}}/100/100/" width="100" height="100" alt="{{alt}}" />
                                    			{{else}}
                                    			    <img itemprop="image" src="{{src}}" width="100" height="100" alt="{{alt}}" />
                                    			{{endif}}
                        		    {{/shop:images}}
                	            </td>   
                                    <td class="description">{{name}}</td>
                                    <td class="price">{{shop:currency}} {{price}}</td>
                                    <td class="qty"><input type="text" name="{{rowid}}[qty]" value="{{qty}}" maxlength="4"></td>
                                    <td class="subtotal">{{shop:currency}} {{subtotal}}</td>
                                    <td class="remove"><a class="" href="{{ url:site }}shop/cart/delete/{{rowid}}">&times;</a></td>				
                		</tr>
                		{{/contents}}
                        <tr class="cart-actions">
                            <td colspan="6">			

                                     Sub total: {{shop:currency}} {{shop:total cart="sub-total"}} 
                                    &nbsp; <input class=""  name="update_cart" type="submit" value="update cart" /><a class="" href="{{url:site}}shop/checkout/">checkout</a>

                            </td>
        		        </tr>
                    </tbody>
        	</table>
            </form>

        {{ else }}
                <?php echo lang('shop:cart:cart_empty'); ?>
        {{ endif }}

    {{ /shop:cart }}

</div>