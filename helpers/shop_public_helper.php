<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */





if (!function_exists('nc_bind_address')) 
{
	/**
	 * $object row not array must be passed
	 * Used only in the event class for sending email
	 */
	function nc_bind_address($address_row,$format ='inline') 
	{
		
		
		if($format =='inline')
		{
			return $address_row->address1.', '.$address_row->address2. ', '.$address_row->city . ' '.$address_row->zip;
		}
		
		return $address_row->address1.', '.$address_row->address2. ', '.$address_row->city . ' '.$address_row->zip;
		
	}

}



// if (!function_exists('orderby_helper'))  -this should only be an admin helper - so i removed it from here

//sf_text
//return form_input($name, $value, 'id="'.$name.'" '.$option);





if (!function_exists('get_country_from_iso2alpha')) 
{
	
	
	/* $code = required either 2 letter country code or for reverse use country name
	 *
	 *
	 * @description: 	-  	Normal mode will get the country name from the ISO 2 Alpha code
	 *			 		-	Reverse will get the country code from the country name
	 *
	 *
	 * @example:
	 * echo get_country_from_iso2alpha( 'AU' ); 				//returns Australia
	 * echo get_country_from_iso2alpha( 'AU' , 'reverse'); 		//returns AU
	 *
	 *
	 * @param $mode  normal|reverse|
	 */
	function get_country_from_iso2alpha( $code,$mode = 'normal',$getarray = FALSE )
	{

			$ci = & get_instance();
		

			$cl = $ci->db->where('enabled',1)->select('name')->select('code2')->get('shop_countries')->result();


			$countryList = array();

			foreach ($cl as $country) 
			{
				$countryList[$country->code2] = $country->name;
			}
 
			
			//return the full array
			if ($getarray)
				return $countryList;
				
			if ($mode == 'reverse') 
				return array_search($code, $countryList); 		

			
			if(isset($countryList[$code]))
					return $countryList[$code];

			//else
			
			return $code;

			
	
	}
}




if (!function_exists('ss_currency_symbol')) 
{

	function ss_currency_symbol() 
	{
		
		$symbols = array(0=>'',1=>'L',2=>'&#36;', 3=>'&#163;', 4=>'&#165;', 5=> 'Rp' ,6=> '&#128;');
		
		return htmlspecialchars_decode($symbols[Settings::get('ss_currency_symbol')]);
		
	}

}



if (!function_exists('nc_format_price')) 
{

	function nc_format_price($price_value, $strict_mode = TRUE, $decimal_points =  2) 
	{
		
		$p_pre = $p_post = "";

		$seperators = array( 0=> ',', 1=> '.', 2=> ' ');
		$symbols = array(0=>'',1=>'L',2=>'&#36;', 3=>'&#163;', 4=>'&#165;', 5=> 'Rp' ,6=> '&#128;');
		
		$layout 		= Settings::get('ss_currency_layout');
		$symbol 		= $symbols[Settings::get('ss_currency_symbol')];
		$thous 			= $seperators[Settings::get('ss_currency_thousand_sep')];
		$dec_point 		= $seperators[Settings::get('ss_currency_decimal_sep')];
		

		$price_value = number_format($price_value,  $decimal_points,  $dec_point,  $thous);	

		$prefix = '';
		$suffix = '';


		if ($layout == '1') 	
			$prefix = $symbol.' ';
		else
			$suffix = ' '.$symbol;
			
		return $prefix.$price_value.$suffix;

	}

}




if (!function_exists('is_deleted')) 
{
	function is_deleted( &$product_object ) 
	{


		if($product_object->date_archived == NULL)
		{
			return FALSE;
		}
		
		return TRUE;
	}

}
