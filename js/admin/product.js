/*
 * This changes the visibility
 *
 */
function sell(i)
{
	id = "#sf_ss_" + i;
    
	var a = $(id).attr("status"); 
    

	
	$.post('shop/admin/products/visibility', { id:i,action:a } ).done(function(data) 
	{			
		var obj = jQuery.parseJSON(data);

		switch(obj.status)
		{
			case 'Sell':
                $(id).attr("class", 'tooltip-s img_icon img_invisible');
				$(id).attr("status", 0);
				break;
			case 'Stop':       
                $(id).attr("class", 'tooltip-s img_icon img_visible');           
				$(id).attr("status", 1);
				break;		
			default:
				alert('Oops, something went wrong. Try refreshing the page');
                $(id).attr("class", 'img_icon img_warning');
				$(id).attr("status", 0);                 
				break;		
		}

	});
}







jQuery(function($){
	

	$(function() {



        
		$('.tab-loader').click(function() {
			

			id = $('#static_product_id').attr('data-pid'); 

            val = $(this).attr('data-load');


		    // Define path to loading image
		    var loading_image = MOD_PATH + '/css/img/gif/ajax-loader.gif';

    
		    // Built temp HTML
		    var str = '<center><div class="loading_image_container"><img src="'+loading_image+'" /></div></center>';
		    
		    
		    // Define the Panel Object
		    var panel = '#' + val + '-tab';

			
			
		    if($(panel).has('.not-loaded').length)
		    {
		    	
	    		// 
			    // Display HTML
			    // 
			    $(panel).html(str); //clear the contents of the panel


		    	$(panel).load('shop/admin/product/load/' + id + '/'+ val , function() 
			    {
				    // This gets executed when the content is loaded
			    	$(panel).show();

				});
			    

		    }

			return false;
		
		});	




	});

});

//#end2