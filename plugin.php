<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Plugin_Shop extends Plugin 
{
	public $version = '1.0.0';
	public $name = array(
		'en' => 'Shop',
	);
	public $description = array(
		'en' => 'Access user and cart information for almost any part of SHOP.',
	);


	/**
	 * Returns a PluginDoc array that PyroCMS uses 
	 * to build the reference in the admin panel
	 *
	 * All options are listed here but refer 
	 * to the Asset plugin for a larger example
	 *
	 * @return array
	 */
	public function _self_doc()
	{
		$info = array(	
			
			'cart' => array(
				'description' => array(
					'en' => 'Display the cart contents.'
				),
				'single' => false,
				'double' => true,
				'variables' => 'id|rowid|name|qty|price|subtotal',
				'attributes' => array(),
			),			
			'currency' => array(
				'description' => array(
					'en' => 'Display the Shop default currency symbol OR format a float value to the Shop currency format.'
				),
				'single' => true,
				'double' => false,
				'variables' => 'id|rowid|name|qty|price|subtotal',
				'attributes' => array(
					'format' => array(
						'type' => 'float',
						'required' => false,
					),
				),
			),	
			'products' => array(
				'description' => array(
					'en' => 'Display a list of ALL products'
				),
				'single' => false,
				'double' => true,
				'variables' => 'id|slug|name|cover_id|price|category_id',
				'attributes' => array(
					'limit' => array(
						'type' => 'Integer',
						'required' => false,
						'default' => '0',
					),
					'order-by' => array(
						'type' => 'String',
						'required' => false,
						'default' => 'date_created',
					),		
					'order-dir' => array(
						'type' => 'String',
						'required' => false,
						'default' => 'asc',
					),	
					'category_id' => array(
						'type' => 'Integer',
						'required' => false,
						'default' => '0',
					),														
				),
			),							
			'product' => array(
				'description' => array(
					'en' => 'Display basic information about a product, Only 1 attribute is required.'
				),
				'single' => false,
				'double' => true,
				'variables' => 'id|slug|name|cover_id|price|category_id',
				'attributes' => array(
					'id' => array(
						'type' => 'Integer',
						'required' => false,
					),
					'slug' => array(
						'type' => 'Integer',
						'required' => false,
					),					
				),
			),	

							
			
		);
	
		return $info;
	}

	/* function dailydeal() - REMOVED - Use from SHOP_Dailydeals plugin - */


	/*
	* Parse Tags in shop
	* Window llows us to create areas on the site simply be defining the html markup with these tags.
	*
	* Given that you assign a name, each window can take the content inside each tag and store it in a BD
	* 
	* Usage:
	* {{ parse:tags }}Content to parse.{{ /parse:tags }}
	*
	* @return string
	*
	public function window()
	{
		$name = $this->attribute('name');
		// We will either take the tag pair content as the
		// parameter or we can take a "string" parameter
		$content = ( ! $this->attribute('string')) ? $this->content() : $this->attribute('string');

		$parser = new Lex_Parser();
		$parser->scope_glue(':');

		//store the 

		$content = "{{ webparts:footer_copyright }}" ; //$name;

		return $parser->parse($content, array(), array($this->parser, 'parser_callback'));
	}
	*/





	

	public function order_is_paid()
	{
		$order_id = $this->attribute('id', NULL);

		if ($this->current_user)
		{
			$this->load->model('shop/orders_m');

			$order = $this->orders_m->get( $order_id );

			if($order->pmt_status =='paid')
			{
				return $this->content();
			}

		}

		return '';

	}

	public function order_is_unpaid()
	{
		$order_id = $this->attribute('id', NULL);


		if ($this->current_user)
		{
			$this->load->model('shop/orders_m');

			$order = $this->orders_m->get( $order_id );

			if($order->pmt_status =='unpaid')
			{
				return $this->content();
			}

		}

		return '';

	}



	public function settings()
	{
		//$product_id = $this->attribute('id', NULL);
		//ss_require_login

		$ret_array[] = array(	
				'allow_guest'=> Settings::get('ss_allow_guest_checkout'),
				'name'=>Settings::get('ss_name'),
				'open'=>Settings::get('nc_open_status'),
				'curr_symbol'=>Settings::get('ss_currency_symbol'),
			);
		
		
		return $ret_array;
	}

	

	/**
	 * For now we only retrieve the symbol, but we should add options for 2 letter code, etc..
	 * @return [type] [description]
	 * 
	 * {{ shop:currency }} - return $ L or pound
	 * {{ shop:currency format="{{total}}" }} - returns the price submited with the currency
	 */
	function currency()
	{

		$ci =& get_instance();
		$ci->load->helper('shop_public');


		$option = $this->attribute( 'get' , 'symbol' );
		$format = $this->attribute( 'format' , 'NO' ); 

		if($format == "NO")
		{
			//then we just need the symbol
			return ss_currency_symbol();
		}

		// ELSE
		return nc_format_price($format);

	}



	function product() 
	{

		$id = $this->attribute('id', '');
		$slug = $this->attribute('slug', '');
		$this->load->model('shop/products_front_m');
	  	
	  	$product = NULL;


		if($id !== '')
		{
			$product =  $this->products_front_m->get($id, 'id');
		}
		else
		{
			$product =  $this->products_front_m->get($slug, 'slug');
		}
		


		if ($product==NULL) 
			return array();

		//if we have used the products_front_m we shouldnt have to check this.
		if (is_deleted($product) || ($product->public == 0)) 
			return array();

		return (array) $product;	

	}





	/**
	 * 
	 * {{ shop:products limit="5" order-by="name" order-dir="asc" category-id="2" }}
	 *	  {{ id }} {{ name }} {{ slug }}
	 * {{ /shop:products }}
	 *
	 * @return	array
	 */
	function products() 
	{
	
		$limit = $this->attribute('limit', "100");
		$offset = $this->attribute( 'offset', "0" ); 
		$order_by = $this->attribute('order-by', 'date_created');
		$order_dir = $this->attribute('order-dir', 'asc');
		$category = $this->attribute('category_id', '0');
		$category = intval($category);
		
		class_exists('products_front_m') OR $this->load->model('shop/products_front_m');
		
		if ($category > 0) 
		{
			$this->products_front_m->where('category_id', $category);
		}

		return $this->products_front_m->limit( $limit , $offset )
					->order_by($order_by, $order_dir)
					->get_all();
	}
	
	

	function cart()
	{

		$CI =& get_instance();
		$CI->load->library('shop/SFCart');

		$ret_array = array();
		$ret_array[] = array(
					'item_count'=> $CI->sfcart->total_items(),
					'contents'=> $CI->sfcart->contents(),
					);

		return $ret_array;	

	}
	
	/**
	 *
	 * @usage: {{ shop:total cart="items" }} 			- Total // of products in cart 
	 * @usage: {{ shop:total cart="sub-total" }} 		- Total cost of products
	 * @usage: {{ shop:total cart="total" }} 			- Total cost of products + shipping
	 * @usage: {{ shop:total cart="shipping" }} 		- Total cost of shipping
	 * @deprecated
	 */
	function total() 
	{
		
		$CI =& get_instance();
		$CI->load->library('shop/SFCart');


		$format = $this->attribute('format', 'NO'); //items is default
		$option = $this->attribute('cart', 'total'); //items is default
		   				
		$price = 0;

		switch ( $option )
		{
			case 'total':
				$price = $CI->sfcart->total();		
				break;
			case 'sub-total':
				$price =number_format( $CI->sfcart->total() , 2) ;			
				break;
			case 'shipping':
				$price =  $CI->sfcart->shipping_total();		
				break;
			case 'items':			
			default:
				$price =  $CI->sfcart->total_items();	

		}


		if(strtoupper($format) == 'YES')
		{
			return nc_format_price($price);
		}

		return $price;
		
	}


}

/* End of file plugin.php */