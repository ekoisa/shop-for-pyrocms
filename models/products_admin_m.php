<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
require_once('products_m.php');

class Products_admin_m extends Products_m
{


	public function __construct() 
	{
		parent::__construct();

	}


	public function get($parm, $method = 'id') 
	{
		
		$product = parent::get($parm,$method); 

		if(!$product)
			return FALSE;

		return $product;
	}


	/**
	 * Get all public and non deleted products
	 * 
	 * @return Array Products Array
	 */
	public function get_all($mode = 'admin') 
	{
		return parent::get_all($mode);
	}





	/**
	 * Create a new product, only some fields are required, the rest uses the default fields, 
	 * when creating a new product, you must first enter the first few req values, save-> then edit the newly created product.
	 *
	 *
	 * @param Array $input Input fields from user, they should be prepped before coming here.
	 */	 
	public function create($input) 
	{
		$_name = strip_tags($input['name']);

		$slug = sf_clean_slug($input['slug']);
		
		$new_slug = $this->get_unique_slug($slug,-1,$_name);


		$to_insert = array(
				'name' => $_name,
				'description' =>  '',
				'related' =>  '', 
				'slug' => $new_slug,
				'price' => $input['price'] ,
				'rrp' => 0.00, 
				'category_id' => 0,
				'created_by' => $this->current_user->id,
				'featured' => 0,
				'searchable' => 1,
				'public' => 0, 
				'date_created' => date("Y-m-d H:i:s"),
				'date_updated' => date("Y-m-d H:i:s"),
				'code' =>  '',
		);
	
		$id =  $this->insert($to_insert); 

		if($id)
		{ 
			//$this->add_to_search($id, $_name, strip_tags($input['description']) );
		}
	

		return $id;
		
	}




	/** 
	 * Store/Edit existing product to db
	 *
	 * @param INT $id
	 * @param Array $input
	 */
	public function edit($id, $input) 
	{
		

		//clean
		//d0n0tr3m0v3th1sf13ld
		if(isset($input['related']) )
		{
			foreach ($input['related'] as $key => $value) 
			{
				if($value == 'd0n0tr3m0v3th1sf13ld')
				{
					unset($input['related'][$key]) ;
				}
			}
		}



		$update_record = array();

		foreach($input as $key => $value)
		{
			$out_value = null;
		

			// we need to check for each field as they need to be handled
			if($this->check_field_req($key,$value, $out_value, $id))
			{
				$update_record[$key] = $out_value;
			}
			 
		}

		//always do this
		//$update_record['featured'] = (isset($input['featured']))?1:0;	
		//$update_record['searchable'] = (isset($input['searchable']))?1:0;						
		$update_record['date_updated'] = date("Y-m-d H:i:s");

				
			
		$result = $this->update($id, $update_record); 
		
		
		if ($result) 
		{
			return $result;	
		}

		return FALSE;
		

	}




	/**
	 * 
	 * @param unknown_type $id The ID of the original product to duplicate
	 * Should we create duplicate price records ?? or letthe admin create new ones ?
	 */
	public function duplicate($id) 
	{


		//
		// Get the original product
		//
		$product = $this->get($id);

		
		//
		// Get the count of items with similar name
		//
		
		//prep of escape char
		$product_name   =   '$product->name';


 		$suffix = $this->db->like('name',$product_name)->get('shop_products')->num_rows();


        $suffix = '-'.$suffix .'';

		$new_slug = $this->get_unique_slug(  sf_clean_slug($product->slug.$suffix)  );



		if ($product==NULL) return FALSE;
		
		$to_insert = array(
				'name' => $product->name,
				'description' => $product->description,				
				'related' => $product->related,
				'slug' => $new_slug,
				'price' => $product->price,			
				'rrp' => $product->rrp,

				'category_id' => $product->category_id,
				'brand_id' => $product->brand_id,

				//shipping
				'height' => $product->height,
				'width' => $product->width,
				'depth' => $product->depth,
				'weight' => $product->weight,
				'req_shipping' => $product->req_shipping,
				'page_design_layout' => $product->page_design_layout,

				'public' =>  0, 
				'views' =>  0, 
				'featured' => $product->featured,
				'searchable' => $product->searchable,
				'date_created' => date("Y-m-d H:i:s"),
				'date_updated' => date("Y-m-d H:i:s"),
				'code' => $product->code,

				'created_by' => $this->current_user->id,
		);
		
		
		$new_id =  $this->insert($to_insert); //returns id


		if($new_id)
		{ 
			//$this->add_to_search($new_id, $product->name, strip_tags($product->description) );
		}


		return $new_id;
		
	}



	

	/**
	 * Admin function
	 * @param  [type] $product_id [description]
	 * @return [type]             [description]
	 */
	public function delete($product_id)
	{	
	
		$del_status =  $this->update($product_id, array('date_archived' => date("Y-m-d H:i:s") ) );

		if( $del_status )
		{
			$this->db->where('product_id',$product_id)->delete('shop_product_files');
		}

		return $del_status;
	
	}



	/**
	 * [check_field_req description]
	 * @param  [type] $key   [description]
	 * @param  [type] $value [description]
	 * @param  [type] $out   [description]
	 * @param  [type] $id    [The Id is needed to check for existing records for slug field that do not match the same ID]
	 * @return [type]        [description]
	 */
	private function check_field_req($key, $value, &$out, $id)
	{
		$pass = FALSE;	

		switch ($key) 
		{
			case 'weight':			
			case 'height':
			case 'width':	
			case 'depth':				
				$out = floatval($value);
				$pass = TRUE;
				break;	

			case 'related':
			case 'related[]':	
				$out = json_encode($value);
				$pass = TRUE;
				break;	
			case 'user_data':
				$out = strip_tags($value);
				$pass = TRUE;
				break;		


			case 'description':		
				$out = strip_tags($value, $this->_description_tags);
				$pass = TRUE;
				break;	

			case 'slug':
				$slug = sf_clean_slug($value);
				$out = $this->get_unique_slug($slug, $id);
				$pass = TRUE;
				break;


			case 'page_design_layout':
			case 'req_shipping':
			case 'inventory_on_hand':
			case 'inventory_low_qty':
			case 'inventory_type':	
			case 'status':		
			case 'category_id':
			case 'featured':
			case 'searchable':
			case 'name':
			case 'price':		
			case 'rrp':		
			case 'code':		
				$out = $value;
				$pass = TRUE;
				break;

			default:
				$pass = FALSE;	
				break;

		}

		return $pass;

		
	}


	
	
	protected function filter_category_list($filter = array())
	{
		$this->reset_query();
		$categories = array();	

		//
		// Get all products in sub categories
		//
		if(isset($filter['category_id']))
		{

			$this->load->model('shop_categories/categories_m','categories_m');

			$cat_id = $filter['category_id'];

			$categories = $this->categories_m->get_children($cat_id);

			//dont forget the parent
			$categories[] =  $this->categories_m->get($cat_id);

		}

		$this->reset_query();	

		return $categories;

	}
	

	protected function _prepare_filter($filter = array()) 
	{

		$new_filter = array();


		if (array_key_exists('visibility', $filter)) 
		{
			$operator = ($filter['visibility'] == 1)? 1 : 0;
			$new_filter['public'] = $operator ;
		}

		return $new_filter;
	}


	/**
	 * Admin Count Filter
	 * 
	 * @param  array  $filter [description]
	 * @return [type]         [description]
	 */
	public function filter_count($filter = array()) 
	{

		$this->reset_query();	
		$categories = $this->filter_category_list($filter);
		$new_filter = $this->_prepare_filter($filter);
		$this->reset_query();



		// Get the count
		foreach ($new_filter as $key => $value) 
		{
			$this->where($key,$value);
		}

		if(trim($filter['search']) != "")
		{
			$this->like('name', trim($filter['search'])); 
		}

		$this->where('date_archived', NULL);

		$this->from($this->_table);


		return $this->count_all_results();

	}

	
	public function filter($filter = array() , $limit, $offset = 0) 
	{

		$this->reset_query();	
		$categories = $this->filter_category_list($filter);
		$new_filter = $this->_prepare_filter($filter);
		$this->reset_query();		

		
		
		foreach ($new_filter as $key => $value) 
		{
			$this->where($key,$value);
		}	

		if(trim($filter['search']) != "")
		{
			$this->like('name', trim($filter['search'])); 
		}


		$this->where('date_archived',NULL);					
		$this->order_by($filter['order_by'],$filter['order_by_order']);
		$this->limit( $limit , $offset );

		return $this->get_all();
	}


	/**
	 * This is only used to help admins find a related product to assign to another product.
	 * 
	 * @param  [type] $term     [description]
	 * @param  [type] $category [description]
	 * @return [type]           [description]
	 */
	public function filter_minimal($term,$category=NULL)
	{

	   $this->db->like('shop_products.name', $term)
				->like('shop_products.slug', $term)
				->or_like('shop_products.meta_desc',$term)
				->or_like('shop_products.code',$term);


				// we max out at 15, if not in list then they should do a better search
				return $this->db->limit(15)->get('shop_products')->result();	
						

	}


	protected function get_unique_slug($slug, $id = -1, $prefix = '')
	{

		// now make sure slug isn not blank
		// we want to pass the name into prefix rather than a random string so it has more contextual meaning
		if(trim($slug) == "")
		{
			$slug = $prefix.$slug; 
		}


		// We want to ommit the current record if we are editing.
		$slug_count = $this->db->where('id !=',$id)->where('slug', $slug )->get( $this->_table )->num_rows();

		if($slug_count > 0)
		{

			$new_slug = $slug.'-'.$slug_count;

			return $this->get_unique_slug($new_slug, $id, $prefix);

		}

		return $slug;

	}	

}