<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
//require_once('products_m.php');

require_once(dirname(__FILE__) . '/' .'products_m.php');

class Products_front_m extends Products_m
{


	public function __construct() 
	{
		parent::__construct();

	}



	/**
	 * 
	 * 
	 * @param  [type]  $parm      [description]
	 * @param  string  $method    [description]
	 * @param  boolean $incr_view [description]
	 * @return [type]             [description]
	 */
	public function get($parm, $method = 'id', $incr_view = FALSE ) 
	{
		
		$product = parent::get($parm,$method); 

		if(!$product)
			return FALSE;


		if(!(group_has_role('shop', 'admin_products')))
		{

			// Make sure product is NOT deleted and is visible to public
			if (($product->date_archived != NULL) || ($product->public == ProductVisibility::Invisible ))
			{
				return FALSE;
			}
		}

		// Add the view count
		if($incr_view)
		{
			$this->viewed($product->id);
		}
		
		return $product;
	}
	
	
	/**
	 * Get all public and non deleted products
	 * 
	 * @return Array Products Array
	 */
	public function get_all($mode = 'public')
	{
		
		if($mode!='public')
		{
			return array();
		}

		return parent::get_all('public');
	}




	/**
	 * Add view counter
	 * 
	 * @param  [type] $product_id [description]
	 * @return [type]             [description]
	 */
	private function viewed($product_id) 
	{
		
		$this->db->select('shop_products.id, shop_products.views');
		
		$item = parent::get_by('shop_products.id', $product_id);

		return  $this->update($product_id,array('views'=>intval( $item->views + 1) )); 

	}
	

	/**
	 * 
	 * @param  [type] $id   [description]
	 * @param  [type] $sold [description]
	 * @return [type]       [description]
	 */
	public function update_inventory( $id , $sold ) 
	{

		//$item = self::get($id);

		// 1 Check if module is installed

		//Check via module pass prod_id return bool

		return TRUE; 
		
	
	}	



	/**
	 * @description This is used for the front end shop, do not use products_m->filter() at the Public site
	 *
	 * @param unknown_type $data
	 * @param unknown_type $limit
	 * @param unknown_type $offset
	 */
	public function filter( $filter, $limit, $offset = 0 ) 
	{
		 

		// Start filtering now
		$this->db->reset_query();


		//Control will be done via a plugin
		$order_by = 'id';
		$order_dir = 'asc';


		// Get the filtered Count
		$items = $this->where('public', ProductVisibility::Visible )
					->where('date_archived', NULL )
					->where('searchable', 1 )
					->order_by($order_by , $order_dir)
					->limit( $limit , $offset )
					->get_all();




		return $items;

	}




	/*count by that counts al products within subcategories as well*/
	public function filter_count($filter = array() )
	{

		
		//
		// add to the existing filter the settings for all front end items
		//  - Must be visible
		//  - must be NOT deleted
		//  - must be searchabe
		//
		$filter['public'] = ProductVisibility::Visible;

		//$filter['deleted'] = ProductStatus::Active;
		$filter['searchable'] = 1;


		// Initialize fields
		$count = 0;


		// we need to do this as we have now collected the categories
		$this->db->reset_query();	


		$this->where('date_archived', NULL );

		//count all products by first category and standard fields
		$count = $this->count_by($filter);

		return $count;
	}


}