<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

 # 
 # SHOP for PyroCMS
 # -----------------------------------------------------------------
 # Author: Salvatore Bordonaro
 # License: See Full license details on the License.txt file
 #
 #

# Admin Routes

# Admin Home Route - 
$route['shop/admin/']		 		= 'admin/shop/test$1';

# StoreFront Routes
$route['shop/products(/:num)']		 		= 'products/index$1';



# Custom Routes
$route['shop/my']		 		    		= 'my/dashboard/index';
