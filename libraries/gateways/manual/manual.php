<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');


class Manual_Gateway {


	public $title = 'Manual - Bank Deposit';
	public $short_title = 'Manual'; //short title is used for transaction line items - much better than storing full nae
	public $desc = 'Bank Deposit';
	public $author = 'Sal Bordonaro';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';
	
	

	public $fields = array(
			array(
				'field' => 'desc',
				'label' => 'Description',
				'rules' => 'trim|max_length[1000]|'
			)

		);

	
	public function __construct() {		}

	


	public function get_param($billing_address, $payable_items, $order, $curr_code='AU') 
	{ 
		return  array();
	}


	public function callback($response) 
	{ 
		
		return NULL;
		
	}

	public function process($order) 
	{ 
		
		return NULL;
		
	}	
	
	public function view_path()
	{
		return 'gateways/'.$this->slug.'/display';
	}
	
}
