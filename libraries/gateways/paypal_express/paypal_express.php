<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');


class Paypal_express_Gateway {


	public $title = 'Paypal Express GateWay';
	public $short_title = 'PayPal'; //short title is used for transaction line items - much better than storing full nae
	public $desc = 'Process Payments via Paypal';
	public $author = 'Sal Bordonaro';
	public $website = 'http://inspiredgroup.com.au';
	public $version = '1.0';
	public $image = '';
	
	

	public $fields = array(
		array(
			'field' => 'options[username]',
			'label' => 'Username',
			'rules' => 'trim|max_length[200]|required'
		),
		array(
			'field' => 'options[password]',
			'label' => 'Password',
			'rules' => 'trim|max_length[100]|required'
		),
		array(
			'field' => 'options[signature]',
			'label' => 'API Signature',
			'rules' => 'trim|max_length[200]|required'
		),		
		array(
			'field' => 'options[test_mode]',
			'label' => 'Test Mode',
			'rules' => 'trim|max_length[100]|numeric'
		),		
		array(
			'field' => 'options[auto]',
			'label' => 'Self submit',
			'rules' => 'trim|max_length[100]|numeric'
		),
	);

	
	public function __construct() {		}


	public function get_param($billing_address, $payable_items, $order, $curr_code='AU') 
	{ 
		
            $params = array(

                    'email' => $billing_address->email,
                    'amount' =>  (float) $order->cost_total,
                    'currency' => $curr_code,
                    'return_url' =>  base_url() . "shop/payment/callback/".  $order->id,
                    'cancel_url' =>  base_url() . "shop/payment/cancel/".  $order->id,
                    'phone' => $billing_address->phone,
                    'postcode' => $billing_address->zip,
                    'country' => $billing_address->country,
                    'city' => $billing_address->city,
                    'region' => $billing_address->state,
                    'address1' => $billing_address->address1,
                    'address2' => $billing_address->address2,           
                    'name' => $billing_address->first_name,     
                    'first_name' => $billing_address->first_name, //test     
                    'last_name' => $billing_address->last_name,   //test
                    'items' => $payable_items                          

            );

		return $params;
	}


	public function callback($response) 
	{ 
		
		return NULL;
		
	}

	public function process($order) 
	{ 
		
		return NULL;
		
	}	

	public function view_path()
	{
		return  'gateways/'.$this->slug.'/display';
	}
	
	
	
}
