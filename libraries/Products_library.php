<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Products_library 
{


	// Private variables.  Do not change!
	private $CI;
	

	public function __construct($params = array())
	{
	
		// Set the super object to a local variable for use later
		$this->CI =& get_instance();

		$this->CI->load->library('settings/settings');

		log_message('debug', "Products Library Class Initialized");
		
	}

	
	
	/**
	 * Set/clear cover image on product
	 * 
	 * @return [type] [description]
	 */
	public function cover_image() 
	{	

		$this->CI->load->model('products_admin_m');


		$response['status'] = JSONStatus::Error;


		if($this->CI->input->post('prod_id') ) 
		{

			$prod_id = intval( $this->CI->input->post('prod_id'));
			
			$img_id =  $this->CI->input->post('img_id') ;
			
			// Step: remove all default/cover flags for this product (should only be 1)
			$data = array('cover' => 0);

			if ( $this->CI->db->where('product_id',$prod_id)->update('shop_images',$data)  ) 
			{
				// Step: set the flag to this one
				$data = array('cover' => 1);

				if ($this->CI->db->where('id',$img_id)->where('product_id',$prod_id)->update('shop_images',$data)  ) 
				{	
					$_img_ = $this->CI->db->where('id',$img_id)->get('shop_images')->row();

					$src = $_img_->src;

					$response['status'] = JSONStatus::Success;
					$response['src'] = $src;
					
					Events::trigger('evt_product_changed', $prod_id);
				} 
			}


		}

		echo json_encode($response);die;

	}




	public function process_for_list(&$products)
	{
		foreach($products as $product)
		{

			$this->process_category($product);

			$this->process_price($product);

			$this->process_inventory($product);

		}
	}



	private function process_category(&$product)
	{

		$product->_category_data = 'Unable to process';
		return;
	/*
		$this->CI->load->model('shop_categories/categories_m','categories_m');

		$category = $this->CI->categories_m->get($product->category_id);

		$cat = ($category->parent_id == 0) ? $category->name :  ss_category_name($category->parent_id) . ' &rarr; ' . $category->name;


		if($product->category_id > 0) 
		{

			$product->_category_data = anchor('admin/shop_categories/categories/edit/' . $product->category_id,  $cat , array('class'=>'')); 
		}
		else
		{
			$product->_category_data = 'no category';
		}
		*/
	}


	private function process_price(&$product)
	{

		$_class = 's_status s_complete'; 
		$_text = nc_format_price($product->price);	

		$product->_price_data = "<span class='".$_class."''>".$_text."</span>";

	}	


	private function process_inventory(&$product)
	{

		//$class_name = 's_low';
		//$class_name = 's_normal';


		$class_name = 's_normal';
		$product->_inventory_data = "<div class='s_status $class_name'>N/A</div>";
		return;
	}
	


	
	public function build_requires_shipping_select($params) 
	{
		 
		$params = array_merge(array('current_id' => 0), $params);
		
		extract($params);
		

		$rs = array(	
						0 	=> 'No'  , 
						1 	=> 'Yes'
						);
		
		$html = '';

		foreach ($rs as $key=>$value) 
		{
			$html .= '<option value="' . $key . '"';
			$html .= $current_id == $key ? ' selected="selected">' : '>';
			$html .= $value . '</option>';
		}
		
	
		return $html;
	}	
}
// END Cart Class
